%global jruby_vendordir %{_datadir}/%{name}/lib
%global jruby_sitedir %{_prefix}/local/share/%{name}/lib
%global rubygems_dir %{_datadir}/rubygems
Name:                jruby
Version:             1.7.22
Release:             5
Summary:             Pure Java implementation of the Ruby interpreter
License:             (CPL or GPLv2+ or LGPLv2+) and BSD and (GPLv2 or Ruby) and (BSD or Ruby)
URL:                 http://jruby.org
Source0:             https://github.com/jruby/jruby/archive/1.7.22.tar.gz
Patch0:              jruby-add-classpath-to-start-script.patch
Patch1:              jruby-include-unbundled-jansi.patch
Patch2:              jruby-remove-rubygems-dirs-definition.patch
Patch3:              jruby-snakeyaml-1.16.patch
Patch4:              jruby-snakeyaml-1.20.patch
Patch5:              CVE-2023-28756.patch
BuildRequires:       maven-local mvn(bsf:bsf) mvn(com.github.jnr:jffi)
BuildRequires:       mvn(com.github.jnr:jffi::native:) mvn(com.github.jnr:jnr-constants)
BuildRequires:       mvn(com.github.jnr:jnr-enxio) mvn(com.github.jnr:jnr-ffi)
BuildRequires:       mvn(com.github.jnr:jnr-netdb) mvn(com.github.jnr:jnr-posix)
BuildRequires:       mvn(com.github.jnr:jnr-unixsocket) mvn(com.github.jnr:jnr-x86asm)
BuildRequires:       mvn(com.headius:coro-mock) mvn(com.headius:invokebinder)
BuildRequires:       mvn(com.headius:options) mvn(com.jcraft:jzlib)
BuildRequires:       mvn(com.facebook:nailgun-server) mvn(jline:jline)
BuildRequires:       mvn(joda-time:joda-time) mvn(junit:junit) mvn(org.apache.ant:ant)
BuildRequires:       mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.felix:org.apache.felix.framework)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-dependency-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-shade-plugin)
BuildRequires:       mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:       mvn(org.codehaus.mojo:exec-maven-plugin)
BuildRequires:       mvn(org.codehaus.mojo:properties-maven-plugin) mvn(org.jruby.extras:bytelist)
BuildRequires:       mvn(org.jruby.jcodings:jcodings) mvn(org.jruby.joni:joni) mvn(org.jruby:yecht)
BuildRequires:       mvn(org.osgi:org.osgi.core) mvn(org.ow2.asm:asm) mvn(org.ow2.asm:asm-analysis)
BuildRequires:       mvn(org.ow2.asm:asm-commons) mvn(org.ow2.asm:asm-util)
BuildRequires:       mvn(org.sonatype.oss:oss-parent:pom:) mvn(org.yaml:snakeyaml) git
BuildRequires:       mvn(org.eclipse.aether:aether-connector-basic)
BuildRequires:       mvn(org.eclipse.aether:aether-transport-wagon)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-http)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-provider-api)
Provides:            ruby(release) = 1.9.3
Provides:            ruby(release) = 1.8.7
Provides:            ruby(runtime_executable)
BuildArch:           noarch
Obsoletes:           %{name}-yecht < %{version}-%{release}
%description
JRuby is a 100% Java implementation of the Ruby programming language.
It is Ruby for the JVM. JRuby provides a complete set of core "builtin"
classes and syntax for the Ruby language, as well as most of the Ruby
Standard Libraries.

%package        devel
Summary:             JRuby development environment
Requires:            %{name} = %{version}-%{release}
%description    devel
Macros for building JRuby-specific libraries.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
Javadoc for %{name}.

%prep
%autosetup -p1
find -name "*.exe" -delete
find -name "*.dll" -delete
find -name ".gitignore" -delete
find -name ".cvsignore" -delete
find lib/ruby/ -name "*.document" -delete
find lib/ruby/ -name "*.rb" -exec sed --in-place "s|^#!/usr/local/bin/ruby||" '{}' \;
find lib/ruby/ -name "*.rb" -exec sed --in-place "s|^#!/usr/bin/env ruby||" '{}' \;
%pom_remove_dep org.jruby:joda-timezones core
%pom_add_dep org.apache.felix:org.apache.felix.framework core
%pom_remove_dep com.headius:unsafe-mock core
%pom_remove_dep com.headius:jsr292-mock core
%pom_remove_plugin :maven-dependency-plugin core
%pom_remove_plugin :tesla-polyglot-maven-plugin lib
%pom_xpath_remove 'pom:dependencies' lib
%pom_remove_plugin :maven-shade-plugin core
%pom_xpath_replace 'pom:dependency[pom:artifactId[text()="jline"]]/pom:scope' '<scope>compile</scope>' ext/readline
%mvn_file :{jruby-core}:jar:: %{name}/@1 %{jruby_vendordir}/%{name}
%mvn_file :{ripper}:jar:: %{name}/@1 %{jruby_vendordir}/ruby/shared/@1
%mvn_file :{readline}:jar:: %{name}/@1 %{jruby_vendordir}/ruby/shared/readline/@1
%mvn_alias org.jruby:jruby-core org.jruby:jruby
grep -rnl martiansoftware | awk '{print $1}' | for line in `xargs`;do sed -i 's/martiansoftware/facebook/g' $line;done

%build
%mvn_build -b

%install
%mvn_install
install -d -m 755 %{buildroot}%{_datadir}
install -p -d -m 755 %{buildroot}%{_datadir}/%{name}/bin
cp -ar lib/* %{buildroot}%{jruby_vendordir}/
install -d -m 755 %{buildroot}%{jruby_vendordir}/jni/{arm-Linux,i386-Linux,x86_64-Linux}
ln -s %{_prefix}/lib/jffi/arm-Linux/libjffi.so %{buildroot}%{jruby_vendordir}/jni/arm-Linux/
ln -s %{_prefix}/lib/jffi/i386-Linux/libjffi.so %{buildroot}%{jruby_vendordir}/jni/i386-Linux/
ln -s %{_prefix}/lib64/jffi/x86_64-Linux/libjffi.so %{buildroot}%{jruby_vendordir}/jni/x86_64-Linux/
ln -s `build-classpath jansi/jansi` %{buildroot}%{jruby_vendordir}/ruby/shared/readline/
xmvn-subst %{buildroot}%{jruby_vendordir}/ruby/shared/readline/
rm %{buildroot}%{jruby_vendordir}/pom*
cp -a bin/{{j,}gem,{j,}irb,jruby} %{buildroot}%{_datadir}/%{name}/bin/
install -d -m 755 %{buildroot}%{_bindir}
ln -s %{_datadir}/%{name}/bin/jgem  %{buildroot}%{_bindir}/gem-jruby
ln -s %{_datadir}/%{name}/bin/jirb  %{buildroot}%{_bindir}/irb-jruby
ln -s %{_datadir}/%{name}/bin/jruby %{buildroot}%{_bindir}/jruby
sed -i 's|$FEDORA_JAVA_OPTS|-Dvendor.dir.general=%{jruby_vendordir}\
                            -Dsite.dir.general=%{jruby_sitedir}\
                            -Dvendor.dir.rubygems=%{rubygems_dir}|' \
    %{buildroot}%{_datadir}/%{name}/bin/jruby*
mkdir -p %{buildroot}%{rubygems_dir}/rubygems/defaults
cp -a lib/ruby/shared/rubygems/defaults/jruby.rb %{buildroot}%{rubygems_dir}/rubygems/defaults/
mkdir -p %{buildroot}%{_rpmconfigdir}/macros.d
cat >> %{buildroot}%{_rpmconfigdir}/macros.d/macros.jruby << \EOF
%%jruby_libdir %%{_datadir}/%{name}/lib/ruby/2.0
%%jruby_vendordir vendor_ruby
%%jruby_vendorlibdir %%{jruby_libdir}/%%{jruby_vendordir}
%%jruby_vendorarchdir %%{jruby_vendorlibdir}
EOF

%files  -f .mfiles
%doc COPYING LICENSE.RUBY LEGAL
%{_bindir}/%{name}
%{_bindir}/gem-jruby
%{_bindir}/irb-jruby
%{_datadir}/%{name}
%{rubygems_dir}/rubygems/defaults/jruby.rb
%exclude %{jruby_vendordir}/ruby/1.9/json*
%exclude %{jruby_vendordir}/ruby/1.9/rdoc*
%exclude %{jruby_vendordir}/ruby/1.9/rake*
%exclude %{jruby_vendordir}/ruby/gems
%exclude %{jruby_vendordir}/ruby/shared/*ubygems*
%exclude %{jruby_vendordir}/ruby/shared/rbconfig

%files devel
%{_rpmconfigdir}/macros.d/macros.jruby

%files javadoc -f .mfiles-javadoc
%doc COPYING LICENSE.RUBY LEGAL

%changelog
* Wed Apr 17 2024 Ge Wang <wang__ge@126.com> - 1.7.22-5
- Replace groupId from martiansoftware to facebook due to nailgun updated

* Fri Jan 26 2024 wangkai <13474090681@163.com> - 1.7.22-4
- Fix CVE-2023-28756

* Fri Aug 18 2023 Ge Wang <wang__ge@126.com> - 1.7.22-3
- Fix build failure due to buildnumber-maven-plugin updated

* Thu Mar 09 2023 yaoxin <yaoxin30@h-partners.com> - 1.7.22-2
- Fix build failure due to snakeyaml update to 1.32

* Mon Aug 24 2020 wangchong <wangchong56@huawei.com> - 1.7.22-1
- package init
